# Crab-Bot - Mascot of Makers @ OU
![crab bot](https://scontent-ort2-1.xx.fbcdn.net/v/t1.6435-9/37909109_898459160341963_3619254926137360384_n.jpg?_nc_cat=102&ccb=1-5&_nc_sid=9267fe&_nc_ohc=OcGEFBGgzwMAX88HPsU&_nc_ht=scontent-ort2-1.xx&oh=fb40a56c1543d96c483ace68fd7ccf20&oe=61D0FCF2)

Crab-bot is a fun interactive robot running on 4 motors and mecanum wheels. He waves his hands wherever he goes and brings joy to everyone around him.

## Technical Description 
Crab-bot is built around the Arduino Mega which interfaces to 4 dedicated motor drivers, one for each wheel. An xbox controller is used as user input to drive crab bot. Two servos run crab-bot's claws. 


## Installation

This repo contains the arduino source code for crab bot along with the necessary dependencies to run it. 
To compile the code clone this repo and add the included folder 'USB_Host_Shield_2.0-master' to your arduino libraries by navigating to sketch > include library > add .ZIP library then selecting the folder previously stated. 

## Contributing
Makers at OU welcomes all OU students to join to work on fun and interactive projects. Contact us on [grizzorgs](https://oaklandu.campuslabs.com/engage/organization/makers-at-oakland-university)
